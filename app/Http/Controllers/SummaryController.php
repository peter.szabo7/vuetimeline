<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;


class SummaryController extends Controller
{
    /**
     * Iterate through the timetable and create a summary string
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function summary(Request $request)
    {
        $result = "";
        $timetable = request('timetable');
        $counter = 0;
        $currentStatus = "";
        $textRow = "";

        foreach($timetable as $hour => $status) {
            if ($hour !== "_cellVariants") {
                if ($status !== $currentStatus) {
                    if ($textRow == "") {
                        $textRow .= "From $hour - ";
                        $currentStatus = $status;
                    } else {
                        $textRow .= "$hour - $currentStatus\n";
                        $currentStatus = $status;
                        $result .= $textRow;
                        $textRow = "From $hour - ";
                    }
                    // this is necessary if only the last hour is different from the previous ones
                    if ($hour == "23:00") {
                        $result .= "From $hour - 24:00 - $status\n";
                    } 
                } else if ($hour == "23:00") {
                    $textRow .= "24:00 - $currentStatus\n";
                    $result .= $textRow;
                }
            }

            if ($status == "Open") {
                $counter += 1;
            }
        }

        $result .= "Total open hours: $counter";

        return response()->json([
            'response' => $result,
            'message' => 'Summary created successfully!'
        ], 200);

    }
}
