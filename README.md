<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

## Risskov timeline - Laravel & VueJs

To get this project running, you should do the following steps:

- Clone this repo
- Go to project folder
- Run "npm install" command
- Run "npm run dev" command
- Run "composer install" command
- Run "composer run post-root-package-install"
- Run "composer run post-create-project-cmd"
- Run "php artisan serve" command
- Project should be up & running at localhost:8000
- Have fun!